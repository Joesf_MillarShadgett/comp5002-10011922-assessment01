﻿using System;

namespace working
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            // Write welcome message with some basic header style
            Console.WriteLine("************************************************************* \n");
            Console.WriteLine("Hello Sir/Madam, welcome to Jossages awesome sausages shop =] \n");
            Console.WriteLine("************************************************************* \n");

            // Declare variables
            double      total           = 0.0;
            bool        addNewNumber    = false;
            string      name            = "";

            // Ask the user for their name, then read and save their input
            Console.WriteLine("\nPlease enter your name.");
            name = Console.ReadLine();

            // Ask the user to type in a number (double), then read and save their input
            Console.WriteLine("\nPlease enter a number with two decimal places.");
            total = double.Parse(Console.ReadLine());

            // Ask the user if they would like to add another number to the original number
            Console.WriteLine("\nWould you like to add a second number? \ntrue = Yes | false = No");
            
            // Read if the user wants to add another number
            addNewNumber = bool.Parse(Console.ReadLine());

            /* If the user wants to add another number, ask them to enter another
            number, read that number and add it to the total. */
            if (addNewNumber)
            {
                Console.WriteLine("\nPlease enter a number with two decimal places.");
                total += double.Parse(Console.ReadLine());
            }

            // Write total pre-GST
            Console.WriteLine($"\nTotal pre-GST: {total}");

            // Calculate & write the GST-inclusive total
            Console.WriteLine($"Total GST-inclusive: {(total * 0.15) + total} \n");

            // Write goodbye message
            Console.WriteLine("Thankyou for shopping with us, please come again.");

            // End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
